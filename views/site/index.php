<?php

/* @var $this yii\web\View */

$this->title = 'Desarrollo web';

?>
<div class="site-index">
    <div class="jumbotron"> 
        <h2><strong>Páginas web</strong> </h2>
        
        <p class="lead">Descubre los frameworks más utilizados para el desarrollo de aplicaciones web!</p>
    </div>
    <div class="row">
            <div class="col-lg-4">
                <h2>Yii2 Framework</h2>

                <p>Yii es un framework PHP rápido, seguro y eficiente.
                   Flexible pero pragmático.
                Funciona nada más sacarlo de la caja.
                Tiene valores predeterminados razonables.</p>

                <p><a class="btn btn-default" href="http://www.yiiframework.com/doc/">Documentación &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Laravel</h2>

                <p>Laravel es un marco de aplicación web con una sintaxis elegante y expresiva. Ya hemos sentado las bases, permitiéndole crear sin preocuparse por las pequeñas cosas.</p>

                <p><a class="btn btn-default" href="https://laravel.com/">Documentación &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>CodeIgniter</h2>

                <p>CodeIgniter es un potente marco PHP con una huella muy pequeña, creado para desarrolladores que necesitan un conjunto de herramientas simple y elegante para crear aplicaciones web con todas las funciones.</p>

                <p><a class="btn btn-default" href="https://www.codeigniter.com/">Documentación &raquo;</a></p>
            </div>
        </div>

    

    
</div>
